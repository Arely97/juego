package com.example.memorama

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_juego.*
import java.util.*
import kotlin.system.exitProcess


class Juego : AppCompatActivity() {
    var botonera = arrayOfNulls<ImageButton>(16)
    var arrayBarajado: ArrayList<*>? = null
    var el0: ImageButton? = null
    var el1: ImageButton? = null
    var el2: ImageButton? = null
    var el3: ImageButton? = null
    var el4: ImageButton? = null
    var el5: ImageButton? = null
    var el6: ImageButton? = null
    var el7: ImageButton? = null
    var el8: ImageButton? = null
    var el9: ImageButton? = null
    var el10: ImageButton? = null
    var el11: ImageButton? = null
    var el12: ImageButton? = null
    var el13: ImageButton? = null
    var el14: ImageButton? = null
    var el15: ImageButton? = null
    var primero: ImageButton? = null
    var segundo: ImageButton? = null
    var textoPuntuacion: TextView? = null
    var puntuacion = 0
    var fondo = 0
    var numeroPrimero = 0
    var numeroSegundo = 0
    var aciertos = 0
    val handler = Handler()
    var bloqueo = false
    var imagenes: IntArray = TODO()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juego)
        cargarImagenes()
        botonesMenu()
        iniciar()
    }
    fun botonesMenu() {
        Reiniciar.setOnClickListener {
            iniciar()
        }
        Salir.setOnClickListener {
            exitProcess(-1)
        }

    }

    fun cargarBotones() {
        el0 = findViewById<View>(R.id.boton00) as ImageButton
        botonera[0] = el0
        el1 = findViewById<View>(R.id.boton01) as ImageButton
        botonera[1] = el1
        el2 = findViewById<View>(R.id.boton02) as ImageButton
        botonera[2] = el2
        el3 = findViewById<View>(R.id.boton03) as ImageButton
        botonera[3] = el3
        el4 = findViewById<View>(R.id.boton04) as ImageButton
        botonera[4] = el4
        el5 = findViewById<View>(R.id.boton05) as ImageButton
        botonera[5] = el5
        el6 = findViewById<View>(R.id.boton06) as ImageButton
        botonera[6] = el6
        el7 = findViewById<View>(R.id.boton07) as ImageButton
        botonera[7] = el7
        el8 = findViewById<View>(R.id.boton08) as ImageButton
        botonera[8] = el8
        el9 = findViewById<View>(R.id.boton09) as ImageButton
        botonera[9] = el9
        el10 = findViewById<View>(R.id.boton10) as ImageButton
        botonera[10] = el10
        el11 = findViewById<View>(R.id.boton11) as ImageButton
        botonera[11] = el11
        el12 = findViewById<View>(R.id.boton12) as ImageButton
        botonera[12] = el12
        el13 = findViewById<View>(R.id.boton13) as ImageButton
        botonera[13] = el13
        el14 = findViewById<View>(R.id.boton14) as ImageButton
        botonera[14] = el14
        el15 = findViewById<View>(R.id.boton15) as ImageButton
        botonera[15] = el15
        textoPuntuacion = findViewById<View>(R.id.textoPuntuacion) as TextView
        textoPuntuacion!!.text = "Puntuación: $puntuacion"
    }


    fun cargarImagenes() {
        imagenes = intArrayOf(
            R.drawable.la0,
            R.drawable.la1,
            R.drawable.la2,
            R.drawable.la3,
            R.drawable.la4,
            R.drawable.la5,
            R.drawable.la6,
            R.drawable.la7
        )
        fondo = R.drawable.fondo
    }

    fun iniciar() {
        arrayBarajado = barajar(imagenes.size * 2)
        cargarBotones()
        for (i in botonera.indices) {
            botonera[i]!!.scaleType = ImageView.ScaleType.CENTER_CROP
            botonera[i]!!.setImageResource(imagenes[arrayBarajado!!.get(i) as Int])
        }
        handler.postDelayed({
            for (i in botonera.indices) {
                botonera[i]!!.scaleType = ImageView.ScaleType.CENTER_CROP
                botonera[i]!!.setImageResource(fondo)
            }
        }, 1000)
        for (i in arrayBarajado?.indices!!) {
            botonera[i]!!.isEnabled = true
            botonera[i]!!.setOnClickListener { if (!bloqueo) comprobar(i, botonera[i]!!) }
        }
        aciertos = 0
        puntuacion = 0
        textoPuntuacion!!.text = "Puntuación: $puntuacion"
    }


    fun barajar(longitud: Int): ArrayList<*> {
        val resultadoA: ArrayList<*> = ArrayList<Int>()
        for (i in 0 until longitud) resultadoA.add((i % longitud / 2) as Nothing)
        resultadoA.shuffle()
        return resultadoA
    }

    fun comprobar(i: Int, imgb: ImageButton) {
        if (primero == null) {
            primero = imgb
            primero!!.setScaleType(ImageView.ScaleType.CENTER_CROP)
            primero!!.setImageResource(imagenes[arrayBarajado!![i] as Int])
            primero!!.setEnabled(false)
            numeroPrimero = arrayBarajado!![i] as Int
        } else {
            bloqueo = true
            imgb.scaleType = ImageView.ScaleType.CENTER_CROP
            imgb.setImageResource(imagenes[arrayBarajado!![i] as Int])
            imgb.isEnabled = false
            numeroSegundo = arrayBarajado!![i] as Int
            if (primero!!.getDrawable().getConstantState() == imgb.drawable.constantState) {
                primero = null
                bloqueo = false
                aciertos++
                puntuacion++
                textoPuntuacion!!.text = "Puntuación: $puntuacion"
                if (aciertos == 8) {
                    val toast =
                        Toast.makeText(applicationContext, "Has  ganado!!", Toast.LENGTH_LONG)
                    toast.show()
                }
            } else {
                handler.postDelayed({
                    primero!!.setScaleType(ImageView.ScaleType.CENTER_CROP)
                    primero!!.setImageResource(R.drawable.fondo)
                    imgb.scaleType = ImageView.ScaleType.CENTER_CROP
                    imgb.setImageResource(R.drawable.fondo)
                    primero!!.setEnabled(true)
                    imgb.isEnabled = true
                    primero = null
                    bloqueo = false
                    if (puntuacion > 0) {
                        textoPuntuacion!!.text = "Puntuación: $puntuacion"
                    }
                }, 1000)
            }
        }
    }


}

