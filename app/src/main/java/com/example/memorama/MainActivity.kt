package com.example.memorama

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        botonJugar.setOnClickListener {
            startActivity(Intent(this, Juego::class.java))
        }
        botonSalir.setOnClickListener {
        exitProcess(-1)
        }
    }
}